document.querySelector(".loader").style.display = "block";
document.querySelector(".films").style.display = "none";

async function get_info(url){
    return await fetch(url)
    .then(res => res.json());
}

async function get_data(url){
    const response = get_info(url)
    .then((films) => {
        console.log(films);
        films.forEach(film => {
            let film_ul = document.createElement('ul');
            let name_film = document.createElement('li');
            name_film.textContent = 'NAME FILM: ' + film.name;
            film_ul.appendChild(name_film);
            let episodeId_film = document.createElement('li');
            episodeId_film.textContent = 'EPISODE ID: ' + film.episodeId;
            film_ul.appendChild(episodeId_film);
            let openingCrawl_film = document.createElement('li');
            openingCrawl_film.textContent = 'OPENING CRAWL: ' + film.openingCrawl;
            film_ul.appendChild(openingCrawl_film);
            let characters_film = document.createElement('li');
            let characters_string_arr = 'CHARACTES: ';
            Promise.all(film.characters.map(characterUrl => fetch(characterUrl).then(response => response.json())))
            .then(characters =>{
                characters.forEach(item =>{
                    characters_string_arr += item.name + ', ';
                })
                characters_film.textContent = characters_string_arr;
                console.log(characters);
                film_ul.appendChild(characters_film);
                document.body.appendChild(film_ul);
                document.querySelector(".loader").style.display = "none";
                document.querySelector(".films").style.display = "block";
            })
        });
    });
    return response;
}

let films = document.querySelector('.films');
let commits = get_data('https://ajax.test-danit.com/api/swapi/films');